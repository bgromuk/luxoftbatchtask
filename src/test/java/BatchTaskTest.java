import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class BatchTaskTest {

    @Test
    public void testBatch() {
        Integer[] testArray1 = {1, 2, 3, 0, 4, 5, 6, 0, 7, 8};
        List<Integer> ints = Arrays.asList(testArray1);

        BatchTask batchTask = new BatchTask();
        Iterator<List<Integer>> batch = batchTask.batch(ints.iterator(), 2);

        assertNotNull(batch);
        List<Integer> next = batch.next();

        assertEquals(next.get(0), testArray1[0]);
        assertEquals(next.get(1), testArray1[1]);

        next = batch.next();

        assertEquals(next.get(0), testArray1[2]);
        assertEquals(next.get(1), testArray1[3]);

        next = batch.next();

        assertEquals(next.get(0), testArray1[4]);
        assertEquals(next.get(1), testArray1[5]);

        next = batch.next();

        assertEquals(next.get(0), testArray1[6]);
        assertEquals(next.get(1), testArray1[7]);

        next = batch.next();

        assertEquals(next.get(0), testArray1[8]);
        assertEquals(next.get(1), testArray1[9]);
    }
}
