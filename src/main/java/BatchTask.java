import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class BatchTask {
    /**
     * Suppose you need to develop an efficient method that split incoming data
     * into series of batches.
     * Write a function that takes an Iterator and batch size,
     * and returns another iterator with batches (Lists) preserving elements order.
     * Make sure method doesn't fetch data eagerly because sometimes iterators are infinite.
     */
    public <T> Iterator<List<T>> batch(final Iterator<T> in, final int batchSize) {
        Iterable<T> iterable = () -> in;
        Stream<T> stream = StreamSupport.stream(iterable.spliterator(), false);

        final AtomicInteger counter = new AtomicInteger(0);
        return stream
                .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / batchSize))
                .values().iterator();

    }

}
